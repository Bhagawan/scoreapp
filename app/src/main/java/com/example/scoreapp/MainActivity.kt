package com.example.scoreapp

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.scoreapp.ui.GameFragment
import com.example.scoreapp.ui.MainFragment
import com.example.scoreapp.ui.NewGameFragment
import com.example.scoreapp.util.Screens
import com.example.scoreapp.viewmodel.MainViewModel
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: MainViewModel
    private lateinit var target: Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment())
                .commitNow()
        }
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        viewModel.getCommands().observe(this) {
            goTo(it)
        }
        setBackground()
    }

    private fun goTo(screen : Screens) = when(screen) {
        Screens.MAIN -> supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.anim_in, R.anim.anim_out)
                .replace(R.id.container, MainFragment())
                .commit()
        Screens.NEW_GAME -> supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.anim_in, R.anim.anim_out)
            .replace(R.id.container, NewGameFragment())
            .commit()
        Screens.GAME -> supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.anim_in, R.anim.anim_out)
            .replace(R.id.container, GameFragment())
            .commit()

    }

    private fun setBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let { findViewById<FrameLayout>(R.id.container).background = BitmapDrawable(resources, it) }
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) { }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }

        }
        Picasso.get().load("http://195.201.125.8/ScoreApp/background.png").into(target)
    }
}