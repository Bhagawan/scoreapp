package com.example.scoreapp.data

import androidx.annotation.Keep

@Keep
data class Player(val name: String, var points: Int)
