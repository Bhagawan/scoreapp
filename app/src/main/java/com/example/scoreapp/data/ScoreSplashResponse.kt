package com.example.scoreapp.data

import androidx.annotation.Keep

@Keep
data class ScoreSplashResponse(val url : String)