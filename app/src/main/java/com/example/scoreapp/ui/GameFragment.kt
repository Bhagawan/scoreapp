package com.example.scoreapp.ui

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.scoreapp.R
import com.example.scoreapp.data.Player
import com.example.scoreapp.databinding.FragmentGameBinding
import com.example.scoreapp.databinding.PopupWinBinding
import com.example.scoreapp.util.SavedPrefs
import com.example.scoreapp.util.adapters.PlayerGameAdapter
import com.example.scoreapp.viewmodel.GameViewModel
import com.example.scoreapp.viewmodel.MainViewModel

class GameFragment: Fragment() {
    private lateinit var binding: FragmentGameBinding
    private lateinit var mainViewModel: MainViewModel
    private lateinit var viewModel: GameViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGameBinding.inflate(layoutInflater)
        mainViewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java]
        viewModel = ViewModelProvider(requireActivity())[GameViewModel::class.java]
        viewModel.initGoal(SavedPrefs.getGoal(binding.root.context))

        binding.recyclerGame.layoutManager = GridLayoutManager(context,2)
        val adapter = PlayerGameAdapter()
        adapter.setCallback(object :PlayerGameAdapter.PlayerGameAdapterCallback {
            override fun increaseScoreForPlayer(player: Player) {
                SavedPrefs.increaseScore(binding.root.context, player)
                viewModel.increasePlayerScore(player)
            }
        })
        adapter.setPlayers(SavedPrefs.getPlayersList(binding.root.context))
        binding.recyclerGame.adapter = adapter

        viewModel.goal.observe(viewLifecycleOwner) {
            SavedPrefs.saveGoal(binding.root.context, it)
            binding.textGoal.text = it.toString()
        }
        viewModel.win.observe(viewLifecycleOwner) {
            it?.let { showWinScreen(it) }
        }
        viewModel.checkPlayers.observe(viewLifecycleOwner) {
            it?.let {
                viewModel.checkPlayers(SavedPrefs.getPlayersList(binding.root.context))
            }
        }
        viewModel.restart.observe(viewLifecycleOwner) {
            it?.let {
                val players = SavedPrefs.getPlayersList(binding.root.context)
                for(p in players) p.points = 0
                adapter.setPlayers(players)
                SavedPrefs.savePlayerList(binding.root.context, players)
            }
        }

        binding.btnGameGoalMinus.setOnClickListener { viewModel.decreaseGoal() }
        binding.btnGameGoalPlus.setOnClickListener { viewModel.increaseGoal() }
        binding.btnGameClose.setOnClickListener { mainViewModel.pressBack() }
        binding.btnGameClear.setOnClickListener { viewModel.restart() }

        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun showWinScreen(winner: String) {
        val popupBinding: PopupWinBinding = PopupWinBinding.inflate(layoutInflater)

        val width = (binding.root.width * 0.75f).toInt()
        val height = LinearLayout.LayoutParams.WRAP_CONTENT

        val popupWindow = PopupWindow(popupBinding.root, width, height, true)
        popupBinding.textWinName.text = winner

        popupBinding.btnWinClose.setOnClickListener {
            popupWindow.dismiss()
            mainViewModel.pressBack()
        }
        popupBinding.btnWinRestart.setOnClickListener {
            viewModel.restart()
            popupWindow.dismiss()
        }

        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = false
        popupBinding.root.findFocus()

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = context?.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    override fun onDestroy() {
        viewModel.destroy()
        super.onDestroy()
    }
}