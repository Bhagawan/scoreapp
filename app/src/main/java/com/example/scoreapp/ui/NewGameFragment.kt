package com.example.scoreapp.ui

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.scoreapp.R
import com.example.scoreapp.data.Player
import com.example.scoreapp.databinding.FragmentNewGameBinding
import com.example.scoreapp.databinding.PopupAddPlayerBinding
import com.example.scoreapp.util.SavedPrefs
import com.example.scoreapp.util.adapters.PlayersListAdapter
import com.example.scoreapp.viewmodel.MainViewModel

class NewGameFragment : Fragment() {
    private lateinit var binding: FragmentNewGameBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewGameBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java]
        binding.recyclerPlayersList.layoutManager = LinearLayoutManager(context)

        val adapter = PlayersListAdapter()
        adapter.setCallback(object : PlayersListAdapter.PlayerListAdapterCallback {
            override fun editPlayer(name: String) {
                addPlayerPopup(name)
            }
        })
        binding.recyclerPlayersList.adapter = adapter

        binding.btnStartGame.setOnClickListener {
            context?.let{ c ->
                SavedPrefs.savePlayerList(c, adapter.getPlayers().map { name -> Player(name, 0)})
                SavedPrefs.saveGoal(c, 1)
            }
            viewModel.pressGame()
        }
        binding.btnCreateGameBack.setOnClickListener { viewModel.pressBack() }
        binding.btnAddPlayer.setOnClickListener { addPlayerPopup(null) }
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun addPlayerPopup(name: String?) {
        val popupBinding: PopupAddPlayerBinding = PopupAddPlayerBinding.inflate(layoutInflater)

        val width = (binding.root.width * 0.75f).toInt()
        val height = LinearLayout.LayoutParams.WRAP_CONTENT

        val popupWindow = PopupWindow(popupBinding.root, width, height, true)
        name?.let { popupBinding.btnPopupAdd.text = context?.getString(R.string.action_redact) }

        popupBinding.btnPopupCancel.setOnClickListener { popupWindow.dismiss() }
        popupBinding.btnPopupAdd.setOnClickListener {
            if(name == null) (binding.recyclerPlayersList.adapter as PlayersListAdapter).addPlayer(popupBinding.editTextName.text.toString())
            else (binding.recyclerPlayersList.adapter as PlayersListAdapter).changePlayer(popupBinding.editTextName.text.toString(), name)
            popupWindow.dismiss()
        }

        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true
        popupBinding.root.findFocus()

        popupWindow.setTouchInterceptor{ _, event ->
            if (event?.action == MotionEvent.ACTION_DOWN) {
                val v = popupBinding.root.findFocus()
                if (v is EditText) {
                    val outRect = Rect()
                    v.getGlobalVisibleRect(outRect)
                    if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                        v.clearFocus()
                        val imm = context?.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                    }
                }
            }
            false
        }

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = context?.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

}