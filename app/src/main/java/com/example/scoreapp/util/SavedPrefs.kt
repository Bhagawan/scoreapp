package com.example.scoreapp.util

import android.content.Context
import com.example.scoreapp.data.Player
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class SavedPrefs {
    companion object {
        fun savePlayerList(context: Context, players: List<Player>) {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            shP.edit().putString("players", Gson().toJson(players)).apply()
        }

        fun getPlayersList(context: Context) : List<Player> {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            return Gson().fromJson(shP.getString("players", "[]"), object: TypeToken<List<Player>>() {}.type)?: emptyList()
        }

        fun saveGoal(context: Context, goal: Int) {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            shP.edit().putInt("goal", goal).apply()
        }

        fun getGoal(context: Context) : Int = context.getSharedPreferences("AppData", Context.MODE_PRIVATE).getInt("goal", 0)

        fun increaseScore(context: Context, player: Player) {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            val g = Gson()
            val playerList = g.fromJson(shP.getString("players", "[]"), object: TypeToken<List<Player>>() {}.type)?: emptyList<Player>()
            if(playerList.contains(player)) playerList[playerList.indexOf(player)].points++
            else {
                for(p in playerList) {
                    if(p.name == player.name) {
                        p.points++
                        break
                    }
                }
            }
            shP.edit().putString("players", Gson().toJson(playerList)).apply()
        }

        fun getId(context: Context) : String {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            var id = shP.getString("id", "default") ?: "default"
            if(id == "default") {
                id = UUID.randomUUID().toString()
                shP.edit().putString("id", id).apply()
            }
            return id
        }
    }
}