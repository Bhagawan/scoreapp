package com.example.scoreapp.util

enum class Screens {
    MAIN, NEW_GAME, GAME
}