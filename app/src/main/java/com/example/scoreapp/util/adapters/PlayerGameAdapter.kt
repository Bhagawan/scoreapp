package com.example.scoreapp.util.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.scoreapp.data.Player
import com.example.scoreapp.databinding.ItemPlayerGameBinding

class PlayerGameAdapter: RecyclerView.Adapter<PlayerGameAdapter.ViewHolder>() {
    private var playerList : List<Player> = emptyList()
    private var adapterCallback : PlayerGameAdapterCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemPlayerGameBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(playerList[position])
    }

    override fun getItemCount(): Int = playerList.size

    fun setCallback(c: PlayerGameAdapterCallback) {
        adapterCallback = c
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setPlayers(players: List<Player>) {
        playerList = players
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val viewHolder: ItemPlayerGameBinding): RecyclerView.ViewHolder(viewHolder.root) {
        fun bind(player: Player) {
            viewHolder.textGameItemName.text = player.name
            viewHolder.textGameItemScore.text = player.points.toString()
            viewHolder.btnGameItemAdd.setOnClickListener {
                player.points++
                viewHolder.textGameItemScore.text = player.points.toString()
                notifyItemChanged(adapterPosition)
                adapterCallback?.increaseScoreForPlayer(player)
            }
        }
    }

    interface PlayerGameAdapterCallback {
        fun increaseScoreForPlayer(player: Player)
    }
}