package com.example.scoreapp.util.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.scoreapp.databinding.ItemPlayerListBinding

class PlayersListAdapter: RecyclerView.Adapter<PlayersListAdapter.ViewHolder>() {
    private var playerList : ArrayList<String> = ArrayList()
    private var adapterCallback : PlayerListAdapterCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemPlayerListBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(playerList[position])
    }

    override fun getItemCount(): Int = playerList.size

    inner class ViewHolder(private val viewHolder: ItemPlayerListBinding): RecyclerView.ViewHolder(viewHolder.root) {
        fun bind(name: String) {
            viewHolder.textListItemName.text = name
            viewHolder.btnPlayerDelete.setOnClickListener {
                playerList.removeAt(adapterPosition)
                notifyItemRemoved(adapterPosition)
            }
            viewHolder.btnPlayerEdit.setOnClickListener { adapterCallback?.editPlayer(name) }
        }
    }

    fun setCallback(c: PlayerListAdapterCallback) {
        adapterCallback = c
    }

    fun changePlayer(name : String, oldName: String) {
        val i = playerList.indexOf(oldName)
        playerList[i] = name
        notifyItemChanged(i)
    }

    fun addPlayer(name: String) {
        playerList.add(name)
        notifyItemInserted(playerList.size)
    }

    fun getPlayers() : List<String> = playerList

    interface PlayerListAdapterCallback {
        fun editPlayer(name: String)
    }
}