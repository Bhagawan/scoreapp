package com.example.scoreapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.scoreapp.data.Player

class GameViewModel: ViewModel() {

    private val goalChannel = MutableLiveData(1)
    val goal : LiveData<Int> = goalChannel

    private val winChannel = MutableLiveData<String?>()
    val win : LiveData<String?> = winChannel

    private val restartChannel = MutableLiveData<Boolean?>()
    val restart : LiveData<Boolean?> = restartChannel

    private val checkChannel = MutableLiveData<Boolean?>()
    val checkPlayers : LiveData<Boolean?> = checkChannel

    fun increasePlayerScore(player: Player) {
        if(player.points >= (goalChannel.value?: 1)) {
            winChannel.postValue(player.name)
        }
    }

    fun initGoal(goal: Int) {
        goalChannel.postValue(goal)
    }

    fun increaseGoal() {
        goalChannel.postValue((goalChannel.value?: 1) + 1)
    }

    fun decreaseGoal() {
        if((goalChannel.value?: 1) > 1) {
            goalChannel.postValue((goalChannel.value?: 2) - 1)
            checkChannel.postValue(true)
        }
    }

    fun restart() {
        restartChannel.postValue(true)
    }

    fun checkPlayers(players: List<Player>) {
        val g = goalChannel.value?: 1
        for(player in players) {
            if(player.points > g) {
                winChannel.postValue(player.name)
                break
            }
        }
    }

    fun destroy() {
        checkChannel.postValue(null)
        winChannel.postValue(null)
        restartChannel.postValue(null)
    }
}