package com.example.scoreapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.scoreapp.util.Screens

class MainViewModel : ViewModel() {
    private val screen = MutableLiveData<Screens>()

    fun getCommands() : LiveData<Screens> = screen

    fun pressStart() = screen.postValue(Screens.NEW_GAME)
    fun pressBack() = screen.postValue(Screens.MAIN)
    fun pressGame() = screen.postValue(Screens.GAME)

}