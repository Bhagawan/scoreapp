package com.example.scoreapp.viewmodel

import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.scoreapp.util.ScoreServerClient
import com.onesignal.OneSignal
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import java.text.SimpleDateFormat
import java.util.*

class ScoreSplashViewModel : ViewModel() {
    private val splashScope = CoroutineScope(Dispatchers.IO)
    private lateinit var request: Job

    private val switchToMain = MutableLiveData<Boolean>()
    val switchToMainListener : LiveData<Boolean> = switchToMain

    private val logoVisibility = MutableLiveData(true)
    val logoVisibilityListener : LiveData<Boolean> = logoVisibility

    private val webAddress = MutableLiveData<String>()
    val webAddressListener : LiveData<String> = webAddress

    fun init(simLanguage: String, id: String) {
        val time = SimpleDateFormat("z", Locale.getDefault()).format(Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.getDefault()).time)
            .replace("GMT", "")
        request = splashScope.async {
            val splash = ScoreServerClient.create().getSplash(
                Locale.getDefault().language,
                simLanguage,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            if (splash.isSuccessful) {
                logoVisibility.postValue(false)

                if (splash.body() != null) {
                    when(splash.body()!!.url) {
                        "no" -> switchToMain.postValue(true)
                        "nopush" -> {
                            OneSignal.disablePush(true)
                            switchToMain.postValue(true)
                        }
                        else -> {
                            webAddress.postValue("https://${splash.body()!!.url}")
                        }
                    }
                } else switchToMain.postValue(true)
            } else switchToMain.postValue(true)
        }
    }

    fun destroy() {
        if(request.isActive) request.cancel()
    }
}